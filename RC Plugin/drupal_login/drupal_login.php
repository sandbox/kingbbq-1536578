<?php

/**
 * Plugin to use with drupal roundcube 
 * This performs an automatic login if accessed from drupal with a special login url. 
 * it will call the URL back for verification. 
 * on logout, it will redirect to @domain of the eMail of the user. 
 *
 * @author Gregor Walter 
 */
class drupal_login extends rcube_plugin
{
  //public $task = 'login';
  
  function init()
  {
    $this->add_hook('startup', array($this, 'startup'));
    $this->add_hook('authenticate', array($this, 'authenticate'));
    $this->add_hook('logout_after', array($this, 'logout_after'));
    
  }

  function startup($args)
  {
    $rcmail = rcmail::get_instance();
	// check if the return url was given…
    if (!empty($_GET['drupal']) 
    ) $args['action'] = 'login';

    return $args;
  }

  function authenticate($args)
  {

	// call the URL back and use the result as password. 
	$url = $_GET['drupal'];
  	$handle = fopen($url, 'r');
  	
	if ($handle)  {

  	  $credentials = fgets($handle);
      fclose($handle);
  	  $password = unserialize($credentials);
      $args['user'] = $_GET['_user'];
      $args['pass'] = $password;
      $args['host'] = 'localhost';
      $args['cookiecheck'] = false;
      $args['valid'] = true;
    }
  
    return $args;
  }
  
  function logout_after($args)
  {
    $rcmail = rcmail::get_instance();

	// get the email of the user for proper redirect. 
    $sql_result = $rcmail->db->query("SELECT t2.email FROM ".$rcmail->config->get('db_table_users')." as t1 join ".$rcmail->config->get('db_table_identities')." as t2 on t1.user_id = t2.user_id where t1.username=? and t2.standard=1", $args['user']);
    if ($sql_result && ($sql_arr = $rcmail->db->fetch_assoc($sql_result))) {
    	list($name,$domain) = explode('@', $sql_arr['email']);
    };
    
    if (!empty($domain)) {
    	$rcmail->output->add_script('top.location.href="http://' . $domain . '";');
    	$rcmail->output->send('plugin');
    }  
    return $args; 
  }


}

